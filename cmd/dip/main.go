// Command for Package dip: looks up the import path for the specified directory
package main

import (
	"fmt"
	"gitlab.com/stu-b-doo/dip"
	"os"
	fp "path/filepath"
)

var helpMsg string = `dip returns the package import path for the specified directory
dip [directory]

directory: for which to return import path. Defaults to current directory.`

func main() {

	// user args
	a := parseArgs()

	// import path for directory
	path, err := dip.DetermineImportPath(a.dir)
	check(err)

	// stdout
	fmt.Println(path)

}

// check error
func check(e error) {
	if e != nil {
		panic(e)
	}
}

type userArgs struct {
	dir string
}

// parseArgs checks validity and returns args in a struct. 
func parseArgs() userArgs {

	// all args optional
	OfferHelp(os.Args, true, 0, helpMsg)

	// directory for which to find import path. Defaults to current directory.
	// get target directory, default to current directory if none supplied
	var dir string
	if len(os.Args) > 1 {
		dir = os.Args[1]
	} else {
		dir = "."
	}

	// resolve directory
	abs, err := fp.Abs(dir)
	check(err)

	return userArgs{dir: abs}

}

// Offerhelp if too few arguments are provided, or the first arg is a request for help
// nReqArgs: number of required arguments.
// ignoreFirst: Ignore the first argument, for example, os.Args includes an additional first argument whereas flag.Args() does not
func OfferHelp(args []string, ignoreFirst bool, nReqArgs int, helpMsg string) {

	reqLen := nReqArgs
	i := 0

	// adjustments if ignoring first item or not
	if ignoreFirst {
		reqLen += 1
		i = 1
	}

	// show help if not enough arguments
	if len(args) < reqLen {
		fmt.Println(helpMsg)
		os.Exit(0)
	}

	// show help if first arg is a request for help
	if len(args) > 1 {
		if args[i] == "help" ||
			args[i] == "--help" ||
			args[i] == "-h" {
			fmt.Println(helpMsg)
			os.Exit(0)
		}
	}
}
