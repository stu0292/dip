package dip

import (
	fp "path/filepath"
	te "testing"
)

// FIXME: behaviour depends on the GOPATH GOROOT env variables!!
func TestDetermineImport(t *te.T) {

	cases := []struct{ in, exp string }{
		{"go/src/my/package", "my/package"},
	}

	for i, c := range cases {
		act, err := DetermineImportPath(fp.FromSlash(c.in))
		if err != nil {
			t.Fatal(err)
		}
		if act != c.exp {
			t.Fatalf("Case %v:\nGot '%s'\nExpected '%s'", i, act, c.exp)
		}
	}
}

// TODO TestAboveBase
// func TestAboveBase(t *te.T) {}
